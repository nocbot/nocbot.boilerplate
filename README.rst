==============================
Boilerplate Saltstack Formula
==============================

.. note::
   - This branch (``master``) is a placeholder branch for the Boilerplate formula.
   - This is an editorconfig_ enabled repository.
   - Formula and Salt Package Manger versioning follows `Semantic Versioning`_ guidelines.

You are most likely looking for one of the following branches:

- development
- integration
- staging
- production

.. _editorconfig: http://editorconfig.org
.. _Semantic Versioning: http://semver.org

